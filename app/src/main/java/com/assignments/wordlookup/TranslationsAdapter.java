package com.assignments.wordlookup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class TranslationsAdapter extends RecyclerView.Adapter<ViewHolder> {
    private ArrayList<String> words;
    private Context context;

    public TranslationsAdapter(ArrayList<String> words, Context context) {
        this.words = words;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(this.context);
        View view = inflater.inflate(R.layout.translation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getTextViewTranslated().setText(words.get(position));
    }

    @Override
    public int getItemCount() {
        return words.size();
    }
}
