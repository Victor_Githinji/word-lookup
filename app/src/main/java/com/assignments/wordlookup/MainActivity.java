package com.assignments.wordlookup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<String> kiswahiliWords, englishWords;
    private EditText editTextEnterWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        kiswahiliWords = new ArrayList<>();
        englishWords = new ArrayList<>();
        Button btnExit = findViewById(R.id.btnExit);
        Button btnTranslate = findViewById(R.id.btnTranslate);
        editTextEnterWord = findViewById(R.id.enterWord);

        loadCsvFile();

        btnTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                translateText();
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadCsvFile() {
        InputStream inputStream = getResources().openRawResource(R.raw.words);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line;
            while ((line = reader.readLine()) != null) {
                try {
                    String[] words = line.split(",");
                    if (words.length == 2) {
                        kiswahiliWords.add(words[0]);
                        englishWords.add(words[1]);
                    }
                } catch (Exception e) {
                    Log.e("Error", e.toString());
                }
            }
        } catch (IOException io) {
            Log.e("Error", io.toString());
        }
    }

    private void translateText() {
        String word = editTextEnterWord.getText().toString().trim();
        ArrayList<String> words = new ArrayList<>();

        if (englishWords.contains(word)) {
            Toast.makeText(this, "Available Translation for " + word, Toast.LENGTH_SHORT).show();

            for (int i = 0; i < englishWords.size(); i++) {
                if (englishWords.get(i).equals(word)) {
                    words.add(kiswahiliWords.get(i));
                }
            }
        } else if (kiswahiliWords.contains(word)) {
            Toast.makeText(this, "Available Translation for " + word, Toast.LENGTH_SHORT).show();
            for (int i = 0; i < kiswahiliWords.size(); i++) {
                if (kiswahiliWords.get(i).equals(word)) {
                    words.add(englishWords.get(i));
                }
            }
        } else {
            Toast.makeText(this, "Word is not Available", Toast.LENGTH_LONG).show();
        }


        TranslationsAdapter adapter = new TranslationsAdapter(words, this);
        RecyclerView view = findViewById(R.id.recyclerView);
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(this));

    }
}
